import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NotebookService} from '../../domain/notebook/service/notebook.service';
import {NoteService} from '../../domain/notebook/service/note.service';
import {Dictionary} from '../../domain/dictionary/dictionary';

@Component({
  selector: 'app-notebook-store',
  templateUrl: './notebook-store.component.html',
  styleUrls: ['./notebook-store.component.scss']
})
export class NotebookStoreComponent implements OnInit {
  // trzeba pobrac liste notesow

  notebooks: Dictionary[];

  constructor(private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private notebookService: NotebookService,
              private noteService: NoteService) {
  }

  ngOnInit(): void {
     this.notebookService.getNotebookDictionaries().subscribe(value => {
       console.log(value);
       this.notebooks = value;
     });
  }


}
