import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotebookStoreComponent} from './notebook-store.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthGuard} from '../../domain/auth/auth-guard';


@NgModule({
  declarations: [NotebookStoreComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AuthGuard],
  exports: [NotebookStoreComponent]
})
export class NotebookStoreModule {
}
