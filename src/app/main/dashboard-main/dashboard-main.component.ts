import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../domain/auth/service/authentication.service';
import {Router} from '@angular/router';
import {AuthorityUtil} from '../../domain/authority/authority-util';

@Component({
  selector: 'app-dashboard-main',
  templateUrl: './dashboard-main.component.html',
  styleUrls: ['./dashboard-main.component.scss']
})
export class DashboardMainComponent implements OnInit {

  isAdmin = false;
  private readonly HOME_PATH = '/home';

  constructor(private router: Router,
              private authenticationService: AuthenticationService) {
    if (this.authenticationService.currentUserPrincipal == null) {
      this.router.navigate([this.HOME_PATH]);
    }
  }

  ngOnInit(): void {
    this.isAdmin = AuthorityUtil.isAdmin();
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate([this.HOME_PATH]);
  }

}
