import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserValidator} from "../../../domain/user/validator/user.validator";
import {Password} from "../../../domain/user/model/password";
import {first} from "rxjs/operators";
import {UserService} from "../../../domain/user/service/user.service";

@Component({
  selector: 'app-password-dialog',
  templateUrl: './password-dialog.component.html',
  styleUrls: ['./password-dialog.component.scss']
})
export class PasswordDialogComponent implements OnInit {

  passwordFormGroup: FormGroup;
  oldPasswordHide = true;
  passwordHide = true;
  confirmationPasswordHide = true;
  submitted = false;
  loading = false;
  errors = [];
  hasErrors = false;

  constructor(private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<PasswordDialogComponent>,
              private userService: UserService) { }

  ngOnInit(): void {
    this.passwordFormGroup = this.createPasswordForm();
  }

  close(): void {
    this.dialogRef.close();
  }

  get form() {
    return this.passwordFormGroup.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.passwordFormGroup.invalid) {
      return;
    }

    this.loading = true;
    this.userService.changePassword(this.buildPassword())
      .pipe(first())
      .subscribe(
        () => this.close(),
        error => {
          for (var err of error.error.errorDetails) {
            this.errors.push(err.code);
          }
          this.hasErrors = true;
          this.loading = false;
        }
      );
  }

  private createPasswordForm(): FormGroup {
    return this.formBuilder.group({
      oldPassword: ['', Validators.required],
      password: ['',
        Validators.compose([
          Validators.required,
          Validators.pattern(UserValidator.passwordRegex)])
      ],
      confirmationPassword: ['', Validators.required]
    }, {
      validator: UserValidator.checkMatchingPasswords('password', 'confirmationPassword')
    });
  }

  private buildPassword(): Password {
    return new Password(
      this.form.oldPassword.value.toString().trim(),
      this.form.password.value.toString().trim(),
      this.form.confirmationPassword.value.toString().trim(),
    );
  }

}
