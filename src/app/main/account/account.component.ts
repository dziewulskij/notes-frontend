import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../domain/user/service/user.service';
import {JwtUtil} from '../../domain/auth/util/jwt-util';
import {UserValidator} from '../../domain/user/validator/user.validator';
import {BaseUser} from '../../domain/user/model/base-user';
import {first} from 'rxjs/operators';
import {User} from '../../domain/user/model/user';
import {MatDialog} from '@angular/material/dialog';
import {PasswordDialogComponent} from './password-dialog/password-dialog.component';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  accountFormGroup: FormGroup;
  accountSubmitted = false;
  errors = [];
  accountFormLoading = false;
  public user: User;

  constructor(private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.loadUserById();
    this.accountFormGroup = this.createAccountForm();
  }

  openPasswordDialog(): void {
    this.dialog.open(PasswordDialogComponent, {
      width: '50%'
    });
  }

  get accountForm() {
    return this.accountFormGroup.controls;
  }

  accountOnSubmit() {
    this.accountSubmitted = true;
    if (this.accountFormGroup.invalid) {
      return;
    }

    this.accountFormLoading = true;
    this.userService.editUserByClient(this.buildBaseUser())
      .pipe(first())
      .subscribe(
        value => {
          this.user = value;
          this.accountFormLoading = false;
        },
        error => {
          for (const err of error.error.errorDetails) {
            this.errors.push(err.code);
          }
          this.accountFormLoading = false;
        }
      );
  }

  private loadUserById() {
    const userId = JwtUtil.getUserId();
    this.userService.getUserById(userId)
      .subscribe(value => this.user = value);
  }

  private createAccountForm(): FormGroup {
    return this.formBuilder.group({
      name: ['',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50)])
      ],
      surname: ['',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50)])
      ],
      email: ['',
        Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          Validators.pattern(UserValidator.emailRegex)])
      ]
    });
  }

  private buildBaseUser(): BaseUser {
    return new BaseUser(
      JwtUtil.getUserId(),
      this.accountForm.name.value.toString().trim(),
      this.accountForm.surname.value.toString().trim(),
      this.accountForm.email.value.toString().trim(),
    );
  }

}
