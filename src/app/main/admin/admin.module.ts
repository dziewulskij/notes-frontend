import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { NotebookManagerComponent } from './notebook-manager/notebook-manager.component';



@NgModule({
  declarations: [AdminComponent, UserManagerComponent, NotebookManagerComponent],
  imports: [
    CommonModule
  ]
})
export class AdminModule { }
