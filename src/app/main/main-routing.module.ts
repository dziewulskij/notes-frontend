import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Route, RouterModule} from '@angular/router';
import {AuthGuard} from '../domain/auth/auth-guard';
import {AccountComponent} from './account/account.component';
import {MainComponent} from './main.component';
import {NotebookStoreComponent} from './notebook-store/notebook-store.component';

const MAIN_ROUTES: Route[] = [
  {
    path: 'main', component: MainComponent, canActivate: [AuthGuard], children: [
      {path: '', pathMatch: 'full', redirectTo: 'store', canActivate: [AuthGuard]},
      {path: 'account', component: AccountComponent, canActivate: [AuthGuard]},
      {path: 'store', component: NotebookStoreComponent, canActivate: [AuthGuard]}
    ]
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(MAIN_ROUTES)
  ],
  exports: [
    RouterModule
  ]
})
export class MainRoutingModule {
}
