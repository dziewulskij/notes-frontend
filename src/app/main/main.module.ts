import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainComponent} from './main.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {FlexModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {AuthGuard} from '../domain/auth/auth-guard';
import {DashboardMainComponent} from './dashboard-main/dashboard-main.component';
import {AccountComponent} from './account/account.component';
import {PasswordDialogComponent} from './account/password-dialog/password-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
  declarations: [
    MainComponent,
    DashboardMainComponent,
    AccountComponent,
    PasswordDialogComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    FlexModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatDialogModule,
    MatSidenavModule,
    MatExpansionModule,
    MatListModule
  ],
  providers: [AuthGuard],
  exports: [
    MainComponent
  ]
})
export class MainModule {
}
