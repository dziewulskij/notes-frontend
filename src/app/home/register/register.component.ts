import {Component, OnInit} from '@angular/core';
import {RegisterUser} from '../../domain/auth/model/register-user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserValidator} from '../../domain/user/validator/user.validator';
import {RegisterService} from '../../domain/auth/service/register.service';
import {first} from 'rxjs/operators';
import {AuthenticationService} from '../../domain/auth/service/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerFormGroup: FormGroup;
  submitted = false;
  errors = [];
  hide = true;
  loading = false;

  constructor(private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private registerService: RegisterService,
              private authenticationService: AuthenticationService) {
    if (this.authenticationService.currentUserPrincipal !== null) {
      this.router.navigate(['/main']);
    }
  }

  ngOnInit(): void {
    this.registerFormGroup = this.createForm();
  }

  get form() {
    return this.registerFormGroup.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerFormGroup.invalid) {
      return;
    }

    this.loading = true;
    this.registerService.registerUser(this.buildRegisterUser())
      .pipe(first())
      .subscribe(
        () => this.router.navigate(['/home']),
        error => {
          for (const err of error.error.errorDetails) {
            this.errors.push(err.code);
          }
          this.loading = false;
        }
      );
  }

  private createForm(): FormGroup {
    return this.formBuilder.group({
      name: ['',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50)])
      ],
      surname: ['',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50)])
      ],
      username: ['',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(30)])
      ],
      email: ['',
        Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          Validators.pattern(UserValidator.emailRegex)])
      ],
      password: ['',
        Validators.compose([
          Validators.required,
          Validators.pattern(UserValidator.passwordRegex)])
      ],
      confirmationPassword: ['', Validators.required]
    }, {
      validator: UserValidator.checkMatchingPasswords('password', 'confirmationPassword')
    });
  }

  private buildRegisterUser(): RegisterUser {
    return new RegisterUser(
      this.form.name.value.toString().trim(),
      this.form.surname.value.toString().trim(),
      this.form.username.value.toString().trim(),
      this.form.email.value.toString().trim(),
      this.form.password.value.toString().trim(),
      this.form.confirmationPassword.value.toString().trim(),
    );
  }

}
