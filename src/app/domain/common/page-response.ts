export class PageResponse<T> {

  constructor(public totalPages: number,
              public pageNumber: number,
              public totalElements: number,
              public content: T[]) {
    this.totalPages = totalPages;
    this.pageNumber = pageNumber;
    this.totalElements = totalElements;
    this.content = content;
  }

}
