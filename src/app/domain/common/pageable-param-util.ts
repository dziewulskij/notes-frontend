import {HttpParams} from '@angular/common/http';

export class PageableParamUtil {

  public static getBasePageableRequestParams(page: number,
                                             size: number): HttpParams {
    return new HttpParams()
      .set('page', String(page))
      .set('size', String(size));
  }

}
