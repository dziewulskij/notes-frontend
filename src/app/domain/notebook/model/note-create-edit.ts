export class NoteCreateEdit {

  constructor(public title: string,
              public subtitle: string,
              public content: string) {
    this.title = title;
    this.subtitle = subtitle;
    this.content = content;
  }

}
