import {BaseNote} from './base-note';

export class Note extends BaseNote {

  constructor(public id: number,
              public title: string,
              public subtitle: string,
              public content: string,
              public createdAt: Date,
              public updatedAt: Date) {
    super(id, title, subtitle, createdAt);
    this.content = content;
    this.updatedAt = updatedAt;
  }

}
