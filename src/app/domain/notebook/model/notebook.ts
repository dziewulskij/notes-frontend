import {Dictionary} from '../../dictionary/dictionary';

export class Notebook {

  constructor(public id: number,
              public title: string,
              public owner: Dictionary,
              public notes: Dictionary[],
              public createdAt: Date,
              public updatedAt: Date) {
    this.id = id;
    this.title = title;
    this.owner = owner;
    this.notes = notes;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

}
