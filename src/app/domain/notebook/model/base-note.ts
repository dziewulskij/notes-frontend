export class BaseNote {

  constructor(public id: number,
              public title: string,
              public subtitle: string,
              public createdAt: Date) {
    this.id = id;
    this.title = title;
    this.subtitle = subtitle;
    this.createdAt = createdAt;
  }

}
