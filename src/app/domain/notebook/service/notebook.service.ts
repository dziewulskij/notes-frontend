import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {BaseNotebook} from '../model/base-notebook';
import {Observable} from 'rxjs';
import {Notebook} from '../model/notebook';
import {Dictionary} from '../../dictionary/dictionary';
import {PageResponse} from '../../common/page-response';
import {PageableParamUtil} from "../../common/pageable-param-util";

@Injectable({
  providedIn: 'root'
})
export class NotebookService {

  private readonly NOTEBOOK_PATH = '/api/notebooks';
  private httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  private headerOptions = {headers: this.httpHeaders};

  constructor(private httpClient: HttpClient) {
  }

  public createNotebook(notebook: BaseNotebook): Observable<Notebook> {
    return this.httpClient.post<Notebook>(
      `${environment.apiUrl}` + this.NOTEBOOK_PATH,
      JSON.stringify(notebook),
      this.headerOptions
    );
  }

  public getNotebookById(id: number): Observable<Notebook> {
    return this.httpClient.get<Notebook>(
      `${environment.apiUrl}` + this.NOTEBOOK_PATH + `/` + id,
      this.headerOptions
    );
  }

  public getAllNotebook(page: number, size: number): Observable<PageResponse<Notebook>> {
    const reqParams = PageableParamUtil.getBasePageableRequestParams(page, size);
    return this.httpClient.get<PageResponse<Notebook>>(
      `${environment.apiUrl}` + this.NOTEBOOK_PATH,
      {headers: this.httpHeaders, params: reqParams}
    );
  }

  public getNotebookDictionaries(): Observable<Dictionary[]> {
    return this.httpClient.get<Dictionary[]>(
      `${environment.apiUrl}` + this.NOTEBOOK_PATH + `/dictionaries`,
      this.headerOptions
    );
  }

  public editNotebookById(id: number, baseNotebook: BaseNotebook): Observable<Notebook> {
    return this.httpClient.put<Notebook>(
      `${environment.apiUrl}` + this.NOTEBOOK_PATH + `/` + id,
      JSON.stringify(baseNotebook),
      this.headerOptions
    );
  }

  public deleteNotebookById(id: number): Observable<void> {
    return this.httpClient.delete<void>(
      `${environment.apiUrl}` + this.NOTEBOOK_PATH + `/` + id,
      this.headerOptions
    );
  }

}
