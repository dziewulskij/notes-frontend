import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {NoteCreateEdit} from '../model/note-create-edit';
import {Note} from '../model/note';
import {PageResponse} from '../../common/page-response';
import {PageableParamUtil} from "../../common/pageable-param-util";

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  private readonly NOTE_PATH = '/api/notes';
  private httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  private headerOptions = {headers: this.httpHeaders};

  constructor(private httpClient: HttpClient) {
  }

  public createNote(notebookId: number, noteCreateEdit: NoteCreateEdit): Observable<Note> {
    return this.httpClient.post<Note>(
      `${environment.apiUrl}` + this.NOTE_PATH + `/notebooks/` + notebookId,
      JSON.stringify(noteCreateEdit),
      this.headerOptions
    );
  }

  public getNoteById(id: number): Observable<Note> {
    return this.httpClient.get<Note>(
      `${environment.apiUrl}` + this.NOTE_PATH + `/` + id,
      this.headerOptions
    );
  }

  public getAllNoteByNotebookId(page: number, size: number, notebookId: number): Observable<PageResponse<Note>> {
    const reqParams = PageableParamUtil.getBasePageableRequestParams(page, size);
    return this.httpClient.get<PageResponse<Note>>(
      `${environment.apiUrl}` + this.NOTE_PATH + `/notebooks/` + notebookId,
      {headers: this.httpHeaders, params: reqParams}
    );
  }

  public editNoteById(id: number, noteCreateEdit: NoteCreateEdit): Observable<Note> {
    return this.httpClient.put<Note>(
      `${environment.apiUrl}` + this.NOTE_PATH + `/` + id,
      JSON.stringify(noteCreateEdit),
      this.headerOptions
    );
  }

  public deleteNoteById(id: number): Observable<void> {
    return this.httpClient.delete<void>(
      `${environment.apiUrl}` + this.NOTE_PATH + `/` + id,
      this.headerOptions
    );
  }

}
