import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {User} from '../model/user';
import {Observable} from 'rxjs';
import {BaseUser} from '../model/base-user';
import {Password} from '../model/password';
import {PageResponse} from '../../common/page-response';
import {UserFilterRequestParam} from '../param/user-filter-request-param';
import {PageableParamUtil} from "../../common/pageable-param-util";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly USER_PATH = '/api/users';
  private httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  private headerOptions = {headers: this.httpHeaders};

  constructor(private httpClient: HttpClient) {
  }

  public getUserById(id: number): Observable<User> {
    return this.httpClient.get<User>(
      `${environment.apiUrl}` + this.USER_PATH + `/` + id,
      this.headerOptions
    );
  }

  public getAllUsers(page: number,
                     size: number,
                     userFilterRequestParam: UserFilterRequestParam): Observable<PageResponse<User>> {
    const reqParams = this.getUserFilterRequestParam(page, size, userFilterRequestParam);
    return this.httpClient.get<PageResponse<User>>(
      `${environment.apiUrl}` + this.USER_PATH,
      {headers: this.httpHeaders, params: reqParams}
    );
  }

  public editUserByClient(baseUser: BaseUser): Observable<User> {
    return this.httpClient.put<User>(
      `${environment.apiUrl}` + this.USER_PATH + `/own`,
      JSON.stringify(baseUser),
      this.headerOptions
    );
  }

  public changePassword(password: Password): Observable<any> {
    return this.httpClient.put(
      `${environment.apiUrl}` + this.USER_PATH + `/own/passwords`,
      JSON.stringify(password),
      this.headerOptions
    );
  }

  private getUserFilterRequestParam(page: number,
                                    size: number,
                                    userFilterRequestParam: UserFilterRequestParam): HttpParams {
    return PageableParamUtil.getBasePageableRequestParams(page, size)
      .set('email', userFilterRequestParam.email)
      .set('username', userFilterRequestParam.username)
      .set('enable', String(userFilterRequestParam.enable))
      .append('authorityIds', userFilterRequestParam.authorityIds.join(', '));
  }

}
