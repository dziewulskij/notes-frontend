import {BaseUser} from './base-user';
import {Dictionary} from '../../dictionary/dictionary';

export class User extends BaseUser {

  constructor(protected id: number,
              public name: string,
              public surname: string,
              public email: string,
              public enable: boolean,
              public username: string,
              public authorities: Dictionary[],
              public createdAt: Date,
              public updatedAt: Date) {
    super(id, name, surname, email);
    this.enable = enable;
    this.username = username;
    this.authorities = authorities;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
