export class Password {

  constructor(private oldPassword: string,
              private password: string,
              private confirmationPassword: string) {
    this.oldPassword = oldPassword;
    this.password = password;
    this.confirmationPassword = confirmationPassword;
  }

}
