export class BaseUser {

  constructor(protected id: number,
              public name: string,
              public surname: string,
              public email: string) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.email = email;
  }

}
