export class UserFilterRequestParam {

  constructor(public email: string,
              public username: string,
              public enable: boolean,
              public authorityIds: number[]) {
    this.email = email;
    this.username = username;
    this.enable = enable;
    this.authorityIds = authorityIds;
  }

}
