import {FormGroup} from '@angular/forms';

export class UserValidator {

  public static readonly emailRegex = '^[a-zA-z0-9]+[\\._a-zA-Z0-9]*@[a-zA-Z0-9]{2,}\\.[a-zA-Z]{2,}[\\.a-zA-Z0-9]*$';
  public static readonly passwordRegex = '^(?=.*\\d)(?=.*[a-z])(?=.*[\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\_\\+\\-\\=])(?=.*[A-Z])(?!.*\\s).{8,}$';

  static checkMatchingPasswords(password: string, confirmationPassword: string) {
    return (formGroup: FormGroup) => {
      const passwordControl = formGroup.controls[password];
      const confirmationPasswordControl = formGroup.controls[confirmationPassword];

      if (confirmationPasswordControl.errors && !confirmationPasswordControl.errors.notMatch) {
        return;
      }

      if (passwordControl.value !== confirmationPasswordControl.value) {
        confirmationPasswordControl.setErrors({notMatch: true});
      } else {
        confirmationPasswordControl.setErrors(null);
      }
    };
  }
}
