import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Dictionary} from '../dictionary/dictionary';

export class AuthorityService {

  private readonly AUTHORITY_PATH = '/api/authorities';
  private headerOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};

  constructor(private httpClient: HttpClient) {
  }

  public getAllAuthorities(): Observable<Dictionary[]> {
    return this.httpClient.get<Dictionary[]>(
      `${environment.apiUrl}` + this.AUTHORITY_PATH,
      this.headerOptions
    );
  }

}
