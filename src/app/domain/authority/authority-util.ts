import {JwtUtil} from '../auth/util/jwt-util';

export class AuthorityUtil {

  public static isAdmin(): boolean {
    return JwtUtil.getAuthorities()
      .some(value => value === 'ROLE_ADMIN');
  }

}
