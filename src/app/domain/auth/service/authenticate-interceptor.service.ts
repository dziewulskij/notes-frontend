import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';


@Injectable()
export class AuthenticateInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(httpRequest: HttpRequest<any>, httpHandler: HttpHandler) {
    if (sessionStorage.getItem('token')) {
      httpRequest = httpRequest.clone({
        setHeaders: {Authorization: sessionStorage.getItem('token')}
      });
    }

    return httpHandler.handle(httpRequest);
  }
}
