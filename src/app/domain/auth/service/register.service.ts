import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {RegisterUser} from '../model/register-user';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private readonly USER_PATH = '/api/users';
  private options = {headers: new HttpHeaders().set('Content-Type', 'application/json')};

  constructor(private httpClient: HttpClient) {
  }

  registerUser(registerUser: RegisterUser) {
    return this.httpClient.post(
      `${environment.apiUrl}` + this.USER_PATH,
      JSON.stringify(registerUser),
      this.options
    );
  }

}
