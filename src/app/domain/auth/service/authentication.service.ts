import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UserPrincipal} from '../model/user-principal';
import {JwtUtil} from '../util/jwt-util';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private payload: any;
  private currentUserSubject: BehaviorSubject<UserPrincipal>;
  public currentUser: Observable<UserPrincipal>;

  constructor(private httpClient: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<UserPrincipal>(this.buildUserPrincipal());
    this.currentUser = this.currentUserSubject.asObservable();
  }

  private buildUserPrincipal(): UserPrincipal {
    this.payload = JwtUtil.decodePayloadJWT();
    if (this.payload === null) {
      return null;
    }

    return new UserPrincipal(
      this.payload.userId,
      sessionStorage.getItem('token'),
      this.payload.authorities,
      this.payload.username);
  }

  public get currentUserPrincipal(): UserPrincipal {
    return this.currentUserSubject.value;
  }

  public login(username: string, password: string) {
    return this.httpClient.post<any>(`${environment.apiUrl}/api/authentication`, {username, password})
      .pipe(map(token => {
          sessionStorage.setItem('token', 'Bearer ' + token.jwt);
          this.currentUserSubject.next(this.buildUserPrincipal());
        })
      );
  }

  public logout() {
    sessionStorage.removeItem('token');
    this.currentUserSubject.next(null);
  }

}
