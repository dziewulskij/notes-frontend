export class RegisterUser {

  constructor(private name: string,
              private surname: string,
              private username: string,
              private email: string,
              private password: string,
              private confirmationPassword: string) {
    this.name = name;
    this.surname = surname;
    this.username = username;
    this.email = email;
    this.password = password;
    this.confirmationPassword = confirmationPassword;
  }

}
