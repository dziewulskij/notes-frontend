export class UserPrincipal {

  constructor(private readonly id: number,
              private readonly jwt: string,
              private readonly authorities: string[],
              private readonly username?: string) {
    this.id = id;
    this.jwt = jwt;
    this.authorities = authorities;
    this.username = username;
  }

  public getId(): number {
    return this.id;
  }

  public getJwt(): string {
    return this.jwt;
  }

  public getAuthorities(): string[] {
    return this.authorities;
  }

  public getUsername(): string {
    return this.username;
  }

}
