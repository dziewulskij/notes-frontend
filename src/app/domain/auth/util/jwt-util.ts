import {Injectable} from '@angular/core';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class JwtUtil {

  constructor() {
  }

  public static decodePayloadJWT(): any {
    try {
      return jwt_decode(this.getToken());
    } catch (Error) {
      return null;
    }
  }

  public static getToken(): string {
    return sessionStorage.getItem('token');
  }

  public static getAuthorities(): string[] {
    return this.decodePayloadJWT().authorities;
  }

  public static getUserId(): number {
    return this.decodePayloadJWT().userId;
  }

}
